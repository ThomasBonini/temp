| **STAFF** |           |
|-----------|------------|
| #announcements | new channel |
| #development | ex #devops |
| #offtopic | pulled up |
| Voice Channel | pulled up |
| #mmo-champion | |
| #overframe | |
| #join-notifications | pulled up |

| **COBALT** |
|-----------|
| #wowdb | 
| #hearthpwn | 
| #mtgsalvation | 
| #minecraftforum | 
| #diablofans | 


| **ALERTS** |
|-----------|
| #alerts | 
| #patch-alerts | 


| **BOTS** |
|-----------|
| #commits | 
| #issues | 
| #rss | 


| **ARCHIVE** |  |
|-----------|------------|
| #general | read only |

*#general to be archived, because I think it causes confusion to have two channels where we talk about development. For non-dev stuff there is #off-topic.*

